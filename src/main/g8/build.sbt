// *****************************************************************************
// Build settings
// *****************************************************************************

inThisBuild(
  Seq(
    scalaVersion := "$scala_version$",
    organization := "$organization$",
    organizationName := "$organization_name$",
    scalafmtOnCompile := false,
    startYear := Some($start_year$),
    licenses += ("MPL-2.0", url("https://www.mozilla.org/en-US/MPL/2.0/")),
    testFrameworks += new TestFramework("munit.Framework"),
    Test / parallelExecution := false,
    dynverSeparator   := "_", // the default `+` is not compatible with docker tags
    scalacOptions ++= Seq(
      "-deprecation",
      "-explain-types",
      "-feature",
      "-language:higherKinds",
      "-language:implicitConversions",
      "-unchecked",
      "-Xfatal-warnings", // Should be enabled if feasible.
      "-Ykind-projector"
    ),
    scalafmtOnCompile := false,
    Compile / console / scalacOptions --= Seq("-Xfatal-warnings"),
    Test / console / scalacOptions --= Seq("-Xfatal-warnings"),
    Test / fork := true
  )
)

// *****************************************************************************
// Projects
// *****************************************************************************

lazy val $name;format="norm,camel"$ =
  project
    .in(file("."))
    .enablePlugins(AutomateHeaderPlugin)
    .settings(
      name := "$name$",
      libraryDependencies ++= Seq(
        library.catsCore,
        library.circeCore,
        library.circeGeneric,
        library.circeParser,
        library.doobieCore,
        library.doobieHikari,
        library.doobiePostgres,
        library.flywayCore,
        library.http4sCirce,
        library.http4sDsl,
        library.http4sEmberClient,
        library.http4sEmberServer,
        library.logback,
        library.postgresql,
        library.pureConfig,
        library.sttpApiSpecCirceYaml,
        library.tapirCats,
        library.tapirCirce,
        library.tapirCore,
        library.tapirHttp4s,
        library.tapirOpenApiDocs,
        library.tapirSwaggerUi,
        library.munit             % Test,
        library.munitCatsEffect   % Test,
        library.munitScalaCheck   % Test,
        library.scalaCheck        % Test
      )
    )

// *****************************************************************************
// Library dependencies
// *****************************************************************************

lazy val library =
  new {
    object Version {
      val cats            = "$cats_version$"
      val circe           = "$circe_version$"
      val doobie          = "$doobie_version$"
      val flyway          = "$flyway_version$"
      val http4s          = "$http4s_version$"
      val logback         = "$logback_version$"
      val munit           = "$munit_version$"
      val munitCatsEffect = "$munit_ce_version$"
      val postgresql      = "$postgresql_version$"
      val pureConfig      = "$pureconfig_version$"
      val scalaCheck      = "$scalacheck_version$"
      val sttpApiSpec     = "$sttp_apispec_version$"
      val tapir           = "$tapir_version$"
    }
    val catsCore             = "org.typelevel"                 %% "cats-core"           % Version.cats
    val circeCore            = "io.circe"                      %% "circe-core"          % Version.circe
    val circeGeneric         = "io.circe"                      %% "circe-generic"       % Version.circe
    val circeParser          = "io.circe"                      %% "circe-parser"        % Version.circe
    val doobieCore           = "org.tpolecat"                  %% "doobie-core"         % Version.doobie
    val doobieHikari         = "org.tpolecat"                  %% "doobie-hikari"       % Version.doobie
    val doobiePostgres       = "org.tpolecat"                  %% "doobie-postgres"     % Version.doobie
    val doobieScalaTest      = "org.tpolecat"                  %% "doobie-scalatest"    % Version.doobie
    val flywayCore           = "org.flywaydb"                  %  "flyway-core"         % Version.flyway
    val http4sCirce          = "org.http4s"                    %% "http4s-circe"        % Version.http4s
    val http4sDsl            = "org.http4s"                    %% "http4s-dsl"          % Version.http4s
    val http4sEmberServer    = "org.http4s"                    %% "http4s-ember-server" % Version.http4s
    val http4sEmberClient    = "org.http4s"                    %% "http4s-ember-client" % Version.http4s
    val logback              = "ch.qos.logback"                %  "logback-classic"     % Version.logback
    val munit                = "org.scalameta"                 %% "munit"               % Version.munit
    val munitCatsEffect      = "org.typelevel"                 %% "munit-cats-effect"   % Version.munitCatsEffect
    val munitScalaCheck      = "org.scalameta"                 %% "munit-scalacheck"    % Version.munit
    val postgresql           = "org.postgresql"                %  "postgresql"          % Version.postgresql
    val pureConfig           = "com.github.pureconfig"         %% "pureconfig-core"     % Version.pureConfig
    val scalaCheck           = "org.scalacheck"                %% "scalacheck"          % Version.scalaCheck
    val sttpApiSpecCirceYaml = "com.softwaremill.sttp.apispec" %% "openapi-circe-yaml"  % Version.sttpApiSpec
    val tapirCats            = "com.softwaremill.sttp.tapir"   %% "tapir-cats"          % Version.tapir
    val tapirCirce           = "com.softwaremill.sttp.tapir"   %% "tapir-json-circe"    % Version.tapir
    val tapirCore            = "com.softwaremill.sttp.tapir"   %% "tapir-core"          % Version.tapir
    val tapirHttp4s          = "com.softwaremill.sttp.tapir"   %% "tapir-http4s-server" % Version.tapir
    val tapirOpenApiDocs     = "com.softwaremill.sttp.tapir"   %% "tapir-openapi-docs"  % Version.tapir
    val tapirSwaggerUi       = "com.softwaremill.sttp.tapir"   %% "tapir-swagger-ui"    % Version.tapir
  }

