lazy val root =
  project
    .in(file("."))
    .settings(
      name := "http4s-tapir",
      Test / test := {
        val _ = (Test / g8Test).toTask("").value
      },
      scriptedLaunchOpts ++= List("-Xms1g", "-Xmx1g", "-XX:ReservedCodeCacheSize=128m", "-Xss4m", "-Dfile.encoding=UTF-8"),
      resolvers += Resolver.url("typesafe", url("https://repo.typesafe.com/typesafe/ivy-releases/"))(Resolver.ivyStylePatterns)
    )
